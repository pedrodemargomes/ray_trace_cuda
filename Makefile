LDLIBS = -lSDL2 -lSDL2_gfx -lm
CFLAGS = -O3

all: sdl_serial sdl_parallel sdl_gpu

sdl_serial: sdl_teste.cpp
	g++ $< $(LDLIBS) $(CFLAGS) -o $@

sdl_parallel: sdl_teste.cpp
	g++ $< $(LDLIBS) $(CFLAGS) -fopenmp -o $@

sdl_gpu: sdl_teste.cu
	nvcc $< $(LDLIBS) $(CFLAGS) -DVIDEO -o $@

sdl_gpu_debug: sdl_teste.cu
	nvcc $< $(LDLIBS) $(CFLAGS) -o $@
