// [header]
// A very basic raytracer example.
// [/header]
// [compile]
// c++ -o raytracer -O3 -Wall raytracer.cpp
// [/compile]
// [ignore]
// Copyright (C) 2012  www.scratchapixel.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// [/ignore]

#include <SDL2/SDL.h>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <vector>
#include <iostream>
#include <cassert>

#ifndef VIDEO
#include "chrono.c"
#endif

#include "Timer.h"

#define WIDTH 1280
#define HEIGHT 720
#define NTHx 28
#define NTHy 28

#define MAX_RAY_DEPTH 3

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

uint8_t pixels[HEIGHT*WIDTH*3];

#if defined __linux__ || defined __APPLE__
// "Compiled for Linux
#else
// Windows doesn't define these values by default, Linux does
#define M_PI 3.141592653589793
#define INFINITY 1e8
#endif

template<typename T>
class Vec3
{
public:
    T x, y, z;
    __host__ __device__ Vec3() : x(T(0)), y(T(0)), z(T(0)) {}
    __host__ __device__ Vec3(T xx) : x(xx), y(xx), z(xx) {}
	__host__  __device__ Vec3(T xx, T yy, T zz) : x(xx), y(yy), z(zz) {}
    __device__ Vec3& normalize()
    {
        T nor2 = length2();
        if (nor2 > 0) {
            T invNor = 1 / sqrt(nor2);
            x *= invNor, y *= invNor, z *= invNor;
        }
        return *this;
    }
    __device__ Vec3<T> operator * (const T &f) const { return Vec3<T>(x * f, y * f, z * f); }
	__device__ Vec3<T> operator * (const Vec3<T> &v) const { return Vec3<T>(x * v.x, y * v.y, z * v.z); }
    __device__ T dot(const Vec3<T> &v) const { return x * v.x + y * v.y + z * v.z; }
    __device__ Vec3<T> operator - (const Vec3<T> &v) const { return Vec3<T>(x - v.x, y - v.y, z - v.z); }
    __device__ Vec3<T> operator + (const Vec3<T> &v) const { return Vec3<T>(x + v.x, y + v.y, z + v.z); }
    __device__ Vec3<T>& operator += (const Vec3<T> &v) { x += v.x, y += v.y, z += v.z; return *this; }
    __device__ Vec3<T>& operator *= (const Vec3<T> &v) { x *= v.x, y *= v.y, z *= v.z; return *this; }
    __device__ Vec3<T> operator - () const { return Vec3<T>(-x, -y, -z); }
    __device__ T length2() const { return x * x + y * y + z * z; }
    __device__ T length() const { return sqrt(length2()); }
};

typedef Vec3<float> Vec3f;

class Sphere
{
public:
    Vec3f center;                           /// position of the sphere
    float radius, radius2;                  /// sphere radius and radius^2
    Vec3f surfaceColor, emissionColor;      /// surface color and emission (light)
    float transparency, reflection;         /// surface transparency and reflectivity
    __host__ __device__ Sphere() {}
    __host__ __device__ Sphere(
        const Vec3f &c,
        const float &r,
        const Vec3f &sc,
        const float &refl = 0,
        const float &transp = 0,
        const Vec3f &ec = 0) :
        center(c), radius(r), radius2(r * r), surfaceColor(sc), emissionColor(ec),
        transparency(transp), reflection(refl)
    { /* empty */ }
    //[comment]
    // Compute a ray-sphere intersection using the geometric solution
    //[/comment]
    __device__ bool intersect(const Vec3f &rayorig, const Vec3f &raydir, float &t0, float &t1) const
    {
        Vec3f l = center - rayorig;
        float tca = l.dot(raydir);
        if (tca < 0) return false;
        float d2 = l.dot(l) - tca * tca;
        if (d2 > radius2) return false;
        float thc = sqrt(radius2 - d2);
        t0 = tca - thc;
        t1 = tca + thc;
        
        return true;
    }
};

__device__ float mix(const float &a, const float &b, const float &mix)
{
    return b * mix + a * (1 - mix);
}

//[comment]
// This is the main trace function. It takes a ray as argument (defined by its origin
// and direction). We test if this ray intersects any of the geometry in the scene.
// If the ray intersects an object, we compute the intersection point, the normal
// at the intersection point, and shade this point using this information.
// Shading depends on the surface property (is it transparent, reflective, diffuse).
// The function returns a color for the ray. If the ray intersects an object that
// is the color of the object at the intersection point, otherwise it returns
// the background color.
//[/comment]
__device__ Vec3f trace(
    int depth,
    const Vec3f &rayorig,
    const Vec3f &raydir,
    const Sphere *spheres,
	int numSpheres)
{
    //if (raydir.length() != 1) std::cerr << "Error " << raydir << std::endl;
    float tnear = INFINITY;
    const Sphere* sphere = NULL;
    // find intersection of this ray with the sphere in the scene
    for (unsigned i = 0; i < numSpheres; ++i) {
        float t0 = INFINITY, t1 = INFINITY;
        if (spheres[i].intersect(rayorig, raydir, t0, t1)) {
            if (t0 < 0) t0 = t1;
            if (t0 < tnear) {
                tnear = t0;
                sphere = &spheres[i];
            }
        }
    }
    // if there's no intersection return black or background color
    if (!sphere)
        return Vec3f(2);
    Vec3f surfaceColor = 0; // color of the ray/surfaceof the object intersected by the ray
    Vec3f phit = rayorig + raydir * tnear; // point of intersection
    Vec3f nhit = phit - sphere->center; // normal at the intersection point
    nhit.normalize(); // normalize normal direction
    // If the normal and the view direction are not opposite to each other
    // reverse the normal direction. That also means we are inside the sphere so set
    // the inside bool to true. Finally reverse the sign of IdotN which we want
    // positive.
    float bias = 1e-4; // add some bias to the point from which we will be tracing
    bool inside = false;
    if (raydir.dot(nhit) > 0) nhit = -nhit, inside = true;
    if ((sphere->transparency > 0 || sphere->reflection > 0) && depth < MAX_RAY_DEPTH) {
        float facingratio = -raydir.dot(nhit);
        // change the mix value to tweak the effect
        float fresneleffect = mix(pow(1 - facingratio, 3), 1, 0.1);
        // compute reflection direction (not need to normalize because all vectors
        // are already normalized)
        Vec3f refldir = raydir - nhit * 2 * raydir.dot(nhit);
        refldir.normalize();
        Vec3f reflection = trace(depth+1, phit + nhit * bias, refldir, spheres, numSpheres);
        Vec3f refraction = 0;
        // the result is a mix of reflection and refraction (if the sphere is transparent)
        if (sphere->transparency) {
            float ior = 1.1, eta = (inside) ? ior : 1 / ior; // are we inside or outside the surface?
            float cosi = -nhit.dot(raydir);
            float k = 1 - eta * eta * (1 - cosi * cosi);
            Vec3f refrdir = raydir * eta + nhit * (eta *  cosi - sqrt(k));
            refrdir.normalize();
            refraction = trace(depth+1, phit - nhit * bias, refrdir, spheres, numSpheres);
        }
        surfaceColor = (
            reflection * fresneleffect +
            refraction * (1 - fresneleffect) ) * sphere->surfaceColor;
    } else {
        // it's a diffuse object, no need to raytrace any further
        for (unsigned i = 0; i < numSpheres; ++i) {
            if (spheres[i].emissionColor.x > 0) {
                // this is a light
                Vec3f transmission = 1;
                Vec3f lightDirection = spheres[i].center - phit;
                lightDirection.normalize();
                for (unsigned j = 0; j < numSpheres; ++j) {
                    if (i != j) {
                        float t0, t1;
                        if (spheres[j].intersect(phit + nhit * bias, lightDirection, t0, t1)) {
                            transmission = 0;
                            break;
                        }
                    }
                }
                surfaceColor += sphere->surfaceColor * transmission *
                max(float(0), nhit.dot(lightDirection)) * spheres[i].emissionColor;
            }
        }
    }
    
    return surfaceColor + sphere->emissionColor;
}

//[comment]
// Main rendering function. We compute a camera ray for each pixel of the image
// trace it and return a color. If the ray hits a sphere, we return the color of the
// sphere at the intersection point, else we return the background color.
//[/comment]
template <int device>
__global__ void render(uint8_t *d_pixels,const Sphere *spheres, const int numSpheres)
{
	int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    
    extern __shared__ Sphere sh_spheres[];
    __syncthreads();
    if(threadIdx.y == 0 and threadIdx.x < numSpheres) {
        sh_spheres[threadIdx.x] = spheres[threadIdx.x];
    }
    __syncthreads();
	
	unsigned width = WIDTH, height = HEIGHT;
    Vec3f image_pix;
    float invWidth = 1 / float(width), invHeight = 1 / float(height);
    float fov = 30, aspectratio = width / float(height);
    float angle = tan(M_PI * 0.5 * fov / 180.);
    // Trace rays

    if(device == 0) {
        if( (x < WIDTH) and (y < HEIGHT) ) {
            float xx = (2 * ((x + 0.5) * invWidth) - 1) * angle * aspectratio;
            float yy = (1 - 2 * ((y + 0.5) * invHeight)) * angle;
            Vec3f raydir(xx, yy, -1);
            raydir.normalize();
            image_pix = trace(0, Vec3f(0), raydir, sh_spheres, numSpheres);
            d_pixels[ 3*(y*WIDTH + x) + 0] = (uint8_t)(min(float(1), image_pix.x) * 255);
            d_pixels[ 3*(y*WIDTH + x) + 1] = (uint8_t)(min(float(1), image_pix.y) * 255);
            d_pixels[ 3*(y*WIDTH + x) + 2] = (uint8_t)(min(float(1), image_pix.z) * 255);
        }
    } else {
        if( (x < WIDTH) and (y < HEIGHT) ) {
            float xx = (2 * ((x + 0.5) * invWidth) - 1) * angle * aspectratio;
            float yy = (1 - 2 * ((y + HEIGHT/2 + 0.5) * invHeight)) * angle;
            Vec3f raydir(xx, yy, -1);
            raydir.normalize();
            image_pix = trace(0, Vec3f(0), raydir, sh_spheres, numSpheres);
            d_pixels[ 3*(y*WIDTH + x) + 0] = (uint8_t)(min(float(1), image_pix.x) * 255);
            d_pixels[ 3*(y*WIDTH + x) + 1] = (uint8_t)(min(float(1), image_pix.y) * 255);
            d_pixels[ 3*(y*WIDTH + x) + 2] = (uint8_t)(min(float(1), image_pix.z) * 255);
        }
    }
    
}



int main() {
	
	dim3 dimBlock(NTHx, NTHy);
	dim3 dimGrid( ceil((double)WIDTH/NTHx) , ceil((double)(HEIGHT/2)/NTHy) );
    
	std::vector<Sphere> spheres;
    // position, radius, surface color, reflectivity, transparency, emission color
	spheres.push_back(Sphere(Vec3f( 0, 0, -20), 4, Vec3f(1.00, 0.32, 0.36), 1, 1));
    spheres.push_back(Sphere(Vec3f(-5, 0, -20),  4, Vec3f(0.90, 0.76, 0.46), 1, 0));
    spheres.push_back(Sphere(Vec3f(2, 0, -50),  3, Vec3f(0.65, 0.77, 0.97), 1, 0.0));
    spheres.push_back(Sphere(Vec3f(-1, 5, -100), 10, Vec3f(0.90, 0.90, 0.90), 1, 0.0));
    
	// light
    spheres.push_back(Sphere(Vec3f( 0.0, 100, -20), 1, Vec3f(0.00, 0.00, 0.00), 0, 0.0, Vec3f(3)));
    spheres.push_back(Sphere(Vec3f( 0.0, 5, 0), 1, Vec3f(0.00, 0.00, 0.00), 0, 0.0, Vec3f(3)));

	// ground
	spheres.push_back(Sphere(Vec3f( 0.0, -10004, -20), 10000, Vec3f(0.20, 0.20, 0.20), 0, 0.0));

    #ifdef VIDEO
	SDL_Window *window;
	SDL_Surface *screenSurface;
	SDL_Surface *surface;
	SDL_Event events;

	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Ray trace", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT,
			SDL_WINDOW_SHOWN);

	screenSurface = SDL_GetWindowSurface(window);

	surface = SDL_CreateRGBSurfaceFrom((void*)pixels,
                WIDTH,
                HEIGHT,
                3 * 8,          // bits per pixel = 24
                WIDTH * 3,  // pitch
                0x0000FF,              // red mask
                0x00FF00,              // green mask
                0xFF0000,              // blue mask
                0);                    // alpha mask (none)

    #endif
	Sphere *d_spheres[2];
    uint8_t *d_pixels[2];
    cudaSetDevice(0);
	cudaMalloc((void**) &d_spheres[0], spheres.size()*sizeof(Sphere));
    cudaMalloc((void**) &d_pixels[0], (HEIGHT/2)*WIDTH*3*sizeof(uint8_t));
    cudaSetDevice(1);
	cudaMalloc((void**) &d_spheres[1], spheres.size()*sizeof(Sphere));
    cudaMalloc((void**) &d_pixels[1], (HEIGHT/2)*WIDTH*3*sizeof(uint8_t));
    
    #ifdef VIDEO
	Timer fps;
	Timer update;
	long int frame = 0;

	fps.start();
    update.start();
    #endif

    cudaStream_t streamMem0, streamMem1;
    cudaStream_t streamKer0, streamKer1;
    cudaStreamCreate(&streamMem0); cudaStreamCreate(&streamMem1);
    cudaStreamCreate(&streamKer0); cudaStreamCreate(&streamKer1);
    
    cudaSetDevice(0);
    cudaMemcpy(d_spheres[0], &spheres[0], spheres.size()*sizeof(Sphere), cudaMemcpyHostToDevice);
    cudaSetDevice(1);
    cudaMemcpy(d_spheres[1], &spheres[0], spheres.size()*sizeof(Sphere), cudaMemcpyHostToDevice);

	bool running = true;
	while(running) {
        // cudaSetDevice(0);
        // cudaMemcpy(d_spheres[0], &spheres[0], spheres.size()*sizeof(Sphere), cudaMemcpyHostToDevice);
        // cudaSetDevice(1);
        // cudaMemcpy(d_spheres[1], &spheres[0], spheres.size()*sizeof(Sphere), cudaMemcpyHostToDevice);
        
        #ifndef VIDEO
        chronometer_t timer;
        while(1) {
            chrono_start(&timer);
        #endif
            cudaSetDevice(0);
            render<0><<< dimGrid, dimBlock, spheres.size()*sizeof(Sphere) >>>(d_pixels[0], d_spheres[0], spheres.size());
            cudaSetDevice(1);
            render<1><<< dimGrid, dimBlock, spheres.size()*sizeof(Sphere) >>>(d_pixels[1], d_spheres[1], spheres.size());
        #ifndef VIDEO
            cudaDeviceSynchronize();
            chrono_stop(&timer);
            std::cout << "Tempo: " << timer.xtotal_ns << "\n";
            chrono_reset(&timer);
        }
        #endif

        cudaSetDevice(0);
        cudaMemcpy(pixels, d_pixels[0], (HEIGHT/2)*WIDTH*3*sizeof(uint8_t), cudaMemcpyDeviceToHost);
        cudaSetDevice(1);
        cudaMemcpy(pixels+(HEIGHT/2)*WIDTH*3*sizeof(uint8_t), d_pixels[1], (HEIGHT/2)*WIDTH*3*sizeof(uint8_t), cudaMemcpyDeviceToHost);
        // std::ofstream ofs("./untitled.ppm", std::ios::out | std::ios::binary);
        // ofs << "P6\n" << WIDTH << " " << HEIGHT << "\n255\n";
        // for (unsigned int i = 0; i < WIDTH * HEIGHT; ++i) {
        //     ofs << pixels[3*i + 0] <<
        //            pixels[3*i + 1] <<
        //            pixels[3*i + 2];
        // }
        // ofs.close();
        #ifdef VIDEO
		SDL_BlitSurface( surface, NULL, screenSurface, NULL );
		SDL_UpdateWindowSurface( window );

		if( update.get_ticks() > 1000 ) {
			std::cout << "Average Frames Per Second: " << frame / ( fps.get_ticks() / 1000.f ) << std::endl;
			update.start(); 
		}

		while(SDL_PollEvent(&events)) {
			if(events.type == SDL_KEYDOWN) {
				switch( events.key.keysym.sym ){
                    case SDLK_LEFT:
                        spheres.front().center.x-=0.1;
                        break;
                    case SDLK_RIGHT:
                        spheres.front().center.x+=0.1;
                        break;
                    case SDLK_UP:
                        spheres.front().center.y+=0.1;
                        break;
                    case SDLK_DOWN:
                        spheres.front().center.y-=0.1;
                        break;
					case SDLK_a:
						spheres.front().center.z+=0.1;
                        break;
					break;
					case SDLK_s:
						spheres.front().center.z-=0.1;
                        break;
					break;
					case SDLK_ESCAPE:
						running = false;
					break;
                    default:
                    break;
                }
			} else if(events.type == SDL_QUIT) {
				running = false;
			}
		}
        frame++;
        #endif
	}


	return 0;
}
